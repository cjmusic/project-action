package org.jeecg.modules.cqb.docFile.service.impl;

import org.jeecg.modules.cqb.docFile.entity.CqbFileInfo;
import org.jeecg.modules.cqb.docFile.mapper.CqbFileInfoMapper;
import org.jeecg.modules.cqb.docFile.service.ICqbFileInfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 附件表
 * @Author: jeecg-boot
 * @Date:   2022-01-06
 * @Version: V1.0
 */
@Service
public class CqbFileInfoServiceImpl extends ServiceImpl<CqbFileInfoMapper, CqbFileInfo> implements ICqbFileInfoService {

}
