package org.jeecg.modules.cqb.docFile.service;

import org.jeecg.modules.cqb.docFile.entity.CqbFileInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 附件表
 * @Author: jeecg-boot
 * @Date:   2022-01-06
 * @Version: V1.0
 */
public interface ICqbFileInfoService extends IService<CqbFileInfo> {

}
