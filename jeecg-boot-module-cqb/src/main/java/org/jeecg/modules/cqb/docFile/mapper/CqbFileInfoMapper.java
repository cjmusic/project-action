package org.jeecg.modules.cqb.docFile.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.cqb.docFile.entity.CqbFileInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 附件表
 * @Author: jeecg-boot
 * @Date:   2022-01-06
 * @Version: V1.0
 */
public interface CqbFileInfoMapper extends BaseMapper<CqbFileInfo> {

}
